#ifndef MATRIXLIB_CPLUSPLUS_MATRIX_LIB_H_
#define MATRIXLIB_CPLUSPLUS_MATRIX_LIB_H_

class Matrix {
  friend Matrix operator*(double num, const Matrix &other);
  friend Matrix operator*(const Matrix &other, double num);
  friend Matrix operator*(const Matrix &matrix_1, const Matrix &matrix_2);
  friend void CopyMatrix(Matrix &matrix_1, const Matrix &matrix_2);

 public:
  Matrix();
  Matrix(int rows, int cols);
  Matrix(const Matrix &other);
  Matrix(Matrix &&other) noexcept;
  ~Matrix();

  int get_rows() const;
  int get_cols() const;
  void set_rows(int value);
  void set_cols(int value);

  bool EqMatrix(const Matrix &other) const;
  void SumMatrix(const Matrix &other);
  void SubMatrix(const Matrix &other);
  void MulNumber(const double num);
  void MulMatrix(const Matrix &other);
  Matrix Transpose() const;
  Matrix CalcComplements() const;
  double Determinant() const;
  Matrix InverseMatrix() const;

  Matrix operator+(const Matrix &other);
  Matrix operator-(const Matrix &other);
  Matrix &operator=(const Matrix &other);
  Matrix &operator=(Matrix &&other) noexcept;
  bool operator==(const Matrix &other);
  Matrix &operator+=(const Matrix &other);
  Matrix &operator-=(const Matrix &other);
  Matrix &operator*=(const Matrix &other);
  Matrix &operator*=(const double num);
  const double &operator()(const int r, const int c) const;
  double &operator()(const int r, const int c);

  bool EqSize(const Matrix &other) const;
  bool IsTheMatrixSquare() const;
  bool MatrixIsNull() const;
  void FillMatrixByArray(const double *arr, const int size_arr);

 private:
  int rows_, cols_;
  double **matrix_;

  Matrix GetMinor(int row, int col) const;
  void Swap(Matrix &other) noexcept;
  void FillArrayWithZeros();
  void InitMatrix(int rows, int cols);
  void MemoryAlloc(int rows, int cols);
  void MemoryFree();
};
#endif  // MATRIXLIB_CPLUSPLUS_MATRIX_LIB_H_
