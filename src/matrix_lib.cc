#include "matrix_lib.h"

#include <math.h>

#include <iostream>

const double kEps(1E-7);

// Constructors
Matrix::Matrix() {
  rows_ = 0;
  cols_ = 0;
  matrix_ = nullptr;
}

Matrix::Matrix(int rows, int cols) : Matrix() {
  try {
    InitMatrix(rows, cols);
  } catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
}

Matrix::Matrix(const Matrix &other) : Matrix(other.rows_, other.cols_) {
  if (!other.MatrixIsNull()) CopyMatrix(*this, other);
}

Matrix::Matrix(Matrix &&other) noexcept { Swap(other); }

Matrix::~Matrix() { MemoryFree(); }

// Accessor and Mutator functions
void Matrix::set_rows(int rows) {
  if (rows < 0)
    throw std::out_of_range("The number of rows cannot be negative");

  if (rows == 0) {
    MemoryFree();
  } else {
    if (matrix_ == nullptr && cols_ > 0) {
      InitMatrix(rows, cols_);
    } else if (matrix_ != nullptr && cols_ > 0) {
      Matrix temp(rows, cols_);
      CopyMatrix(temp, *this);
      *this = temp;
    }
  }
  rows_ = rows;
}

void Matrix::set_cols(int cols) {
  if (cols < 0)
    throw std::out_of_range("The number of rows cannot be negative");

  if (cols == 0) {
    MemoryFree();
  } else {
    if (matrix_ == nullptr && rows_ > 0) {
      InitMatrix(rows_, cols);
    } else if (matrix_ != nullptr && rows_ > 0) {
      Matrix temp(rows_, cols);
      CopyMatrix(temp, *this);
      *this = temp;
    }
  }
  cols_ = cols;
}

int Matrix::get_rows() const { return rows_; }

int Matrix::get_cols() const { return cols_; }

// Main functions
bool Matrix::EqMatrix(const Matrix &other) const {
  bool result = true;
  if (matrix_ == nullptr || other.matrix_ == nullptr)
    throw std::invalid_argument("Memory is not allocated for the matrix");

  if (EqSize(other)) {
    for (int i = 0; i < rows_; ++i) {
      if (result == false) break;
      for (int j = 0; j < cols_; ++j) {
        if (abs(matrix_[i][j] - other.matrix_[i][j]) > kEps) {
          result = false;
          break;
        }
      }
    }
  } else {
    result = false;
  }
  return result;
}

void Matrix::SumMatrix(const Matrix &other) {
  if (!EqSize(other)) throw std::invalid_argument("Unequal size of matrices");

  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      matrix_[i][j] = matrix_[i][j] + other.matrix_[i][j];
    }
  }
}

void Matrix::SubMatrix(const Matrix &other) {
  if (!EqSize(other)) throw std::invalid_argument("Unequal size of matrices");

  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      matrix_[i][j] = matrix_[i][j] - other.matrix_[i][j];
    }
  }
}

void Matrix::MulNumber(const double num) {
  if (MatrixIsNull()) throw std::out_of_range("Memory not allocated");

  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      matrix_[i][j] *= num;
    }
  }
}

void Matrix::MulMatrix(const Matrix &other) {
  if (cols_ != other.rows_)
    throw std::invalid_argument(
        "The number of columns in the first matrix is not equal to the number "
        "of rows in the second");

  Matrix result(rows_, other.cols_);
  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < other.cols_; ++j) {
      for (int k = 0; k < other.rows_; ++k) {
        result.matrix_[i][j] += matrix_[i][k] * other.matrix_[k][j];
      }
    }
  }
  *this = result;
}

Matrix Matrix::Transpose() const {
  Matrix result(cols_, rows_);
  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      result.matrix_[j][i] = matrix_[i][j];
    }
  }
  return result;
}

Matrix Matrix::CalcComplements() const {
  if (!IsTheMatrixSquare()) throw std::invalid_argument("Matrix is not square");
  Matrix result(*this);

  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      Matrix minor_matrix = GetMinor(i, j);
      result.matrix_[i][j] =
          ((i + j) % 2 == 0 ? 1 : -1) * minor_matrix.Determinant();
    }
  }
  return result;
}

double Matrix::Determinant() const {
  if (!IsTheMatrixSquare()) throw std::invalid_argument("Matrix is not square");

  double determinant = 0.0;
  if (rows_ == 1) {
    determinant = matrix_[0][0];
  } else if (rows_ == 2) {
    determinant =
        (matrix_[0][0] * matrix_[1][1]) - (matrix_[0][1] * matrix_[1][0]);
  } else if (rows_ > 2) {
    for (int i = 0; i < rows_; ++i) {
      Matrix minor_matrix = GetMinor(i, 0);
      determinant += matrix_[i][0] * pow(-1, i) * minor_matrix.Determinant();
    }
  }
  return determinant;
}

Matrix Matrix::InverseMatrix() const {
  double determinant = Determinant();
  if (determinant == 0) throw std::invalid_argument("Determinant is 0");

  Matrix temp = CalcComplements();
  Matrix result = temp.Transpose();
  result.MulNumber(1 / determinant);
  return result;
}

// Operators
Matrix operator*(double num, const Matrix &other) {
  Matrix result(other);
  result.MulNumber(num);
  return result;
}

Matrix operator*(const Matrix &other, double num) {
  Matrix result(other);
  result.MulNumber(num);
  return result;
}

Matrix operator*(const Matrix &matrix_1, const Matrix &matrix_2) {
  Matrix result(matrix_1);
  try {
    result.MulMatrix(matrix_2);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    result.MemoryFree();
    result.rows_ = 0;
    result.cols_ = 0;
  }
  return result;
}

Matrix Matrix::operator+(const Matrix &other) {
  Matrix result(*this);
  try {
    result.SumMatrix(other);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    result.MemoryFree();
    result.rows_ = 0;
    result.cols_ = 0;
  }
  return result;
}

Matrix Matrix::operator-(const Matrix &other) {
  Matrix result(*this);
  try {
    result.SubMatrix(other);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    result.MemoryFree();
    result.rows_ = 0;
    result.cols_ = 0;
  }
  return result;
}

bool Matrix::operator==(const Matrix &other) { return EqMatrix(other); }

Matrix &Matrix::operator+=(const Matrix &other) {
  try {
    SumMatrix(other);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
  return *this;
}

Matrix &Matrix::operator-=(const Matrix &other) {
  try {
    SubMatrix(other);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
  return *this;
}

Matrix &Matrix::operator*=(const Matrix &other) {
  try {
    MulMatrix(other);
  } catch (const std::invalid_argument &e) {
    std::cerr << e.what() << std::endl;
  }
  return *this;
}

Matrix &Matrix::operator*=(const double num) {
  MulNumber(num);
  return *this;
}

const double &Matrix::operator()(const int r, const int c) const {
  if (r < 0 || c < 0 || r >= rows_ || c >= cols_) {
    throw std::out_of_range("You are trying to access the allocated memory");
  }
  return matrix_[r][c];
}

double &Matrix::operator()(const int r, const int c) {
  if (r < 0 || c < 0 || r >= rows_ || c >= cols_) {
    throw std::out_of_range("You are trying to access the allocated memory");
  }
  return matrix_[r][c];
}
Matrix &Matrix::operator=(const Matrix &other) {
  Matrix temp(other);
  MemoryFree();
  Swap(temp);
  return *this;
}

Matrix &Matrix::operator=(Matrix &&other) noexcept {
  Matrix temp(std::move(other));
  MemoryFree();
  Swap(temp);
  return *this;
}

// Additional functions
Matrix Matrix::GetMinor(int row, int col) const {
  Matrix result(rows_ - 1, cols_ - 1);
  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      if (i != row && j != col) {
        result.matrix_[i < row ? i : i - 1][j < col ? j : j - 1] =
            matrix_[i][j];
      }
    }
  }
  return result;
}

void Matrix::Swap(Matrix &other) noexcept {
  rows_ = other.rows_;
  cols_ = other.cols_;
  matrix_ = other.matrix_;
  other.cols_ = 0;
  other.matrix_ = nullptr;
}

void Matrix::InitMatrix(int rows, int cols) {
  if (rows < 0 || cols < 0) throw std::out_of_range("Invalid matrix size");

  rows_ = rows;
  cols_ = cols;
  matrix_ = nullptr;

  if (rows_ > 0 && cols_ > 0) {
    MemoryAlloc(rows_, cols_);
    FillArrayWithZeros();
  }
}

void Matrix::FillArrayWithZeros() {
  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      matrix_[i][j] = 0;
    }
  }
}

void Matrix::MemoryFree() {
  if (matrix_ != nullptr) {
    for (int i = 0; i < rows_; ++i) {
      delete[] matrix_[i];
    }
    delete[] matrix_;
    matrix_ = nullptr;
  }
}

void Matrix::MemoryAlloc(const int rows, const int cols) {
  double **temp_arr = new double *[rows] {};
  for (int i = 0; i < rows; ++i) {
    temp_arr[i] = new double[cols];
  }
  matrix_ = temp_arr;
}

void CopyMatrix(Matrix &matrix_1, const Matrix &matrix_2) {
  matrix_1.FillArrayWithZeros();
  for (int i = 0; i < matrix_1.rows_; ++i) {
    if (i == matrix_2.rows_) {
      break;
    }
    for (int j = 0; j < matrix_1.cols_; ++j) {
      if (j == matrix_2.cols_) {
        break;
      }
      matrix_1.matrix_[i][j] = matrix_2.matrix_[i][j];
    }
  }
}

bool Matrix::EqSize(const Matrix &other) const {
  return rows_ == other.rows_ && cols_ == other.cols_;
}

bool Matrix::IsTheMatrixSquare() const { return cols_ == rows_; }

bool Matrix::MatrixIsNull() const { return (matrix_ == nullptr); }

void Matrix::FillMatrixByArray(const double *arr, const int size_arr) {
  if (matrix_ == nullptr)
    throw std::domain_error("Matrix memory not allocated");

  int arr_count_el = 0;
  for (int i = 0; i < rows_; ++i) {
    for (int j = 0; j < cols_; ++j) {
      if (arr_count_el >= size_arr) {
        matrix_[i][j] = 0.0;
      } else {
        matrix_[i][j] = arr[arr_count_el];
      }
      arr_count_el++;
    }
  }
}
