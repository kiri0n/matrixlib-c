#include <gtest/gtest.h>

#include "matrix_lib.h"

TEST(Constructor, default_con) {
  Matrix temp;
  ASSERT_EQ(temp.get_rows(), 0);
  ASSERT_EQ(temp.get_cols(), 0);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Constructor, with_parameters_con_1) {
  Matrix temp(3, 3);
  ASSERT_EQ(temp.get_rows(), 3);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_FALSE(temp.MatrixIsNull());
}

TEST(Constructor, with_parameters_con_2) {
  Matrix temp_1(-1, 3);
  ASSERT_EQ(temp_1.get_rows(), 0);
  ASSERT_EQ(temp_1.get_cols(), 0);
  Matrix temp_2(-1, 0);
  ASSERT_EQ(temp_2.get_rows(), 0);
  ASSERT_EQ(temp_2.get_cols(), 0);
}

TEST(Constructor, with_parameters_con_3) {
  Matrix temp(0, 0);
  ASSERT_EQ(temp.get_rows(), 0);
  ASSERT_EQ(temp.get_cols(), 0);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Constructor, copy_con_1) {
  Matrix temp_master;
  Matrix temp_replica(temp_master);
  ASSERT_EQ(temp_replica.get_rows(), 0);
  ASSERT_EQ(temp_replica.get_cols(), 0);
  ASSERT_TRUE(temp_replica.MatrixIsNull());
}

TEST(Constructor, copy_con_2) {
  Matrix temp_master(3, 3);
  Matrix temp_replica(temp_master);
  ASSERT_EQ(temp_replica.get_rows(), 3);
  ASSERT_EQ(temp_replica.get_cols(), 3);
  ASSERT_FALSE(temp_replica.MatrixIsNull());
  ASSERT_TRUE(temp_replica.EqMatrix(temp_master));
}

TEST(Constructor, replace_con_1) {
  Matrix temp_master(3, 3);
  Matrix temp_replica = std::move(temp_master);
  ASSERT_EQ(temp_replica.get_rows(), 3);
  ASSERT_EQ(temp_replica.get_cols(), 3);
  ASSERT_FALSE(temp_replica.MatrixIsNull());
}

TEST(Constructor, replace_con_2) {
  Matrix temp_master(3, 3);
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  temp_master.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  Matrix temp_replica = std::move(temp_master);
  Matrix expectation(3, 3);
  expectation.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_EQ(temp_replica.get_rows(), 3);
  ASSERT_EQ(temp_replica.get_cols(), 3);
  ASSERT_FALSE(temp_replica.MatrixIsNull());
  ASSERT_TRUE(temp_replica.EqMatrix(expectation));
}

TEST(Constructor, replace_con_3) {
  Matrix temp_master(0, 0);
  Matrix temp_replica = std::move(temp_master);
  ASSERT_EQ(temp_replica.get_rows(), 0);
  ASSERT_EQ(temp_replica.get_cols(), 0);
  ASSERT_TRUE(temp_replica.MatrixIsNull());
}

TEST(Mutator, setter_1) {
  Matrix temp;
  temp.set_rows(5);
  ASSERT_EQ(temp.get_rows(), 5);
  ASSERT_EQ(temp.get_cols(), 0);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Mutator, setter_2) {
  Matrix temp;
  temp.set_cols(5);
  ASSERT_EQ(temp.get_rows(), 0);
  ASSERT_EQ(temp.get_cols(), 5);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Mutator, setter_3) {
  Matrix temp;
  temp.set_rows(2);
  temp.set_cols(3);
  ASSERT_EQ(temp.get_rows(), 2);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_FALSE(temp.MatrixIsNull());
}

TEST(Mutator, setter_4) {
  Matrix temp;
  temp.set_rows(2);
  ASSERT_THROW(temp.set_cols(-2), std::exception);
  ASSERT_EQ(temp.get_rows(), 2);
  ASSERT_EQ(temp.get_cols(), 0);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Mutator, setter_5) {
  Matrix temp;
  ASSERT_THROW(temp.set_rows(-2), std::exception);
  temp.set_cols(2);
  ASSERT_EQ(temp.get_rows(), 0);
  ASSERT_EQ(temp.get_cols(), 2);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Mutator, setter_6) {
  Matrix temp(0, 3);
  ASSERT_TRUE(temp.MatrixIsNull());
  ASSERT_NO_THROW(temp.set_rows(2));

  ASSERT_EQ(temp.get_rows(), 2);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_FALSE(temp.MatrixIsNull());

  ASSERT_NO_THROW(temp.set_cols(2));
  ASSERT_EQ(temp.get_rows(), 2);
  ASSERT_EQ(temp.get_cols(), 2);
  ASSERT_FALSE(temp.MatrixIsNull());
}

TEST(Mutator, setter_7) {
  Matrix temp(3, 3);
  ASSERT_NO_THROW(temp.set_rows(0));
  ASSERT_EQ(temp.get_rows(), 0);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Mutator, setter_8) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  Matrix temp(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_NO_THROW(temp.set_rows(4));
  ASSERT_EQ(temp.get_rows(), 4);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_FALSE(temp.MatrixIsNull());
  ASSERT_NO_THROW(temp.set_cols(2));
  ASSERT_EQ(temp.get_rows(), 4);
  ASSERT_EQ(temp.get_cols(), 2);
  ASSERT_FALSE(temp.MatrixIsNull());
}

TEST(Mutator, setter_9) {
  Matrix temp(3, 3);
  ASSERT_NO_THROW(temp.set_cols(0));
  ASSERT_EQ(temp.get_rows(), 3);
  ASSERT_EQ(temp.get_cols(), 0);
  ASSERT_TRUE(temp.MatrixIsNull());
}

TEST(Mutator, setter_10) {
  Matrix temp(0, 0);
  ASSERT_NO_THROW(temp.set_cols(3));
  ASSERT_EQ(temp.get_rows(), 0);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_TRUE(temp.MatrixIsNull());
  ASSERT_NO_THROW(temp.set_rows(2));
  ASSERT_EQ(temp.get_rows(), 2);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_FALSE(temp.MatrixIsNull());
}

TEST(Mutator, setter_11) {
  double arr_1[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double arr_3[] = {1.0, 2.0, 4.0, 5.0};
  Matrix temp(3, 3);
  Matrix expect_1(2, 3);
  Matrix expect_2(2, 2);
  temp.FillMatrixByArray(arr_1, sizeof(arr_1) / sizeof(arr_1[0]));
  expect_1.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  expect_2.FillMatrixByArray(arr_3, sizeof(arr_3) / sizeof(arr_3[0]));
  ASSERT_NO_THROW(temp.set_rows(2));
  ASSERT_EQ(temp.get_rows(), 2);
  ASSERT_EQ(temp.get_cols(), 3);
  ASSERT_FALSE(temp.MatrixIsNull());
  ASSERT_TRUE(temp.EqMatrix(expect_1));
  ASSERT_NO_THROW(temp.set_cols(2));
  ASSERT_EQ(temp.get_rows(), 2);
  ASSERT_EQ(temp.get_cols(), 2);
  ASSERT_FALSE(temp.MatrixIsNull());
  ASSERT_TRUE(temp.EqMatrix(expect_2));
}

TEST(EqMatrix, eqmatrix_1) {
  int cols = 2, rows = 2;
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  Matrix temp(rows, cols);
  Matrix temp2(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  bool result = temp.EqMatrix(temp2);
  ASSERT_TRUE(result);
}

TEST(EqMatrix, eqmatrix_2) {
  int cols = 2, rows = 2;
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  Matrix temp(rows, cols);
  Matrix temp2(rows + 1, cols + 1);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  bool result = temp.EqMatrix(temp2);
  ASSERT_FALSE(result);
}

TEST(EqMatrix, eqmatrix_3) {
  int cols = 2, rows = 2;
  double arr_1[] = {1.0, 2.0, 3.0, 4.0};
  double arr_2[] = {4.0, 3.0, 2.0, 1.0};
  Matrix temp(rows, cols);
  Matrix temp2(rows, cols);
  temp.FillMatrixByArray(arr_1, sizeof(arr_1) / sizeof(arr_1[0]));
  temp2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  bool result = temp.EqMatrix(temp2);
  ASSERT_FALSE(result);
}

TEST(EqMatrix, eqmatrix_4) {
  Matrix temp;
  Matrix temp2;
  ASSERT_THROW(temp.EqMatrix(temp2), std::exception);
}

TEST(SumSub, sum_matrix_1) {
  int cols = 3, rows = 3;
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_res[] = {2.0, 4.0, 6.0, 8.0};
  Matrix temp(rows, cols);
  Matrix temp2(rows, cols);
  Matrix temp3(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  ASSERT_NO_THROW(temp.SumMatrix(temp2));
  bool result = temp.EqMatrix(temp3);
  ASSERT_TRUE(result);
}

TEST(SumSub, sum_matrix_2) {
  int cols = 3, rows = 3;
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_res[] = {2.0, 4.0, 6.0, 8.0};
  Matrix temp(rows, cols);
  Matrix temp2(rows, cols - 1);
  Matrix temp3(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  ASSERT_THROW(temp.SumMatrix(temp2), std::invalid_argument);
  bool result = temp.EqMatrix(temp3);
  ASSERT_FALSE(result);
}

TEST(SumSub, sub_matrix_1) {
  int cols = 3, rows = 3;
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  Matrix temp(rows, cols);
  Matrix temp2(rows, cols);
  Matrix temp3(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_NO_THROW(temp.SubMatrix(temp2));
  bool result = temp.EqMatrix(temp3);
  ASSERT_TRUE(result);
}

TEST(SumSub, sub_matrix_2) {
  int cols = 3, rows = 3;
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_res[] = {0.0, 0.0, 0.0, 0.0};
  Matrix temp(rows, cols);
  Matrix temp2(rows, cols + 1);
  Matrix temp3(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  ASSERT_THROW(temp.SubMatrix(temp2), std::invalid_argument);
  bool result = temp.EqMatrix(temp3);
  ASSERT_FALSE(result);
}

TEST(Multiplication, number_1) {
  int cols = 3, rows = 3;
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_res[] = {3.0, 6.0, 9.0, 12.0};
  double num = 3.0;
  Matrix temp(rows, cols);
  Matrix temp3(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  temp.MulNumber(num);
  bool result = temp.EqMatrix(temp3);
  ASSERT_TRUE(result);
}

TEST(Multiplication, number_2) {
  int cols = 3, rows = 3;
  double arr[] = {0.0, 0.0, 0.0, 0.0};
  double arr_res[] = {0.0, 0.0, 0.0, 0.0};
  double num = 3.0;
  Matrix temp(rows, cols);
  Matrix temp3(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  temp.MulNumber(num);
  bool result = temp.EqMatrix(temp3);
  ASSERT_TRUE(result);
}

TEST(Multiplication, number_3) {
  int cols = 3, rows = 3;
  double arr[] = {1.0, 1.0, -20.0, 5.0000001};
  double arr_res[] = {-3.0, -3.0, 60.0, -15.0000003};
  double num = -3.0;
  Matrix temp(rows, cols);
  Matrix temp3(rows, cols);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  temp.MulNumber(num);
  bool result = temp.EqMatrix(temp3);
  ASSERT_TRUE(result);
}

TEST(Multiplication, number_4) {
  Matrix temp;
  ASSERT_THROW(temp.MulNumber(2.0), std::exception);
}

TEST(Multiplication, matrix_1) {
  int cols = 3, rows = 3;
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {-1.0, 2.0, -3.0, 5.0, 4.0, 8.0, 1.0, 2.0, 2.0};
  double arr_result[] = {12.0, 16.0, 19.0, 27.0, 40.0, 40.0, 42.0, 64.0, 61.0};
  Matrix temp_(rows, cols);
  Matrix temp_2(rows, cols);
  Matrix temp_3(rows, cols);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_3.FillMatrixByArray(arr_result,
                           sizeof(arr_result) / sizeof(arr_result[0]));
  ASSERT_NO_THROW(temp_.MulMatrix(temp_2));
  bool result = temp_.EqMatrix(temp_3);
  ASSERT_TRUE(result);
}

TEST(Multiplication, matrix_2) {
  double arr[] = {1.0, 2.0, 1.0, 0.0, 1.0, 2.0};
  double arr_2[] = {1.0, 0.0, 0.0, 1.0, 1.0, 1.0};
  double arr_result[] = {2.0, 3.0, 2.0, 3.0};
  Matrix temp_(2, 3);
  Matrix temp_2(3, 2);
  Matrix temp_3(2, 2);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_3.FillMatrixByArray(arr_result,
                           sizeof(arr_result) / sizeof(arr_result[0]));
  ASSERT_NO_THROW(temp_.MulMatrix(temp_2));
  bool result = temp_.EqMatrix(temp_3);
  ASSERT_TRUE(result);
}

TEST(Multiplication, matrix_3) {
  double arr[] = {1.0, 0.0, 0.0, 1.0, 1.0, 1.0};
  double arr_2[] = {1.0, 2.0, 1.0, 0.0, 1.0, 2.0};
  double arr_result[] = {1.0, 2.0, 1.0, 0.0, 1.0, 2.0, 1.0, 3.0, 3.0};
  Matrix temp_(3, 2);
  Matrix temp_2(2, 3);
  Matrix temp_3(3, 3);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_3.FillMatrixByArray(arr_result,
                           sizeof(arr_result) / sizeof(arr_result[0]));
  ASSERT_NO_THROW(temp_.MulMatrix(temp_2));
  bool result = temp_.EqMatrix(temp_3);
  ASSERT_TRUE(result);
}

TEST(Multiplication, matrix_4) {
  double arr[] = {2.0, 3.0, 6.0, 5.0};
  double arr_2[] = {8.0, 9.0, 7.0, 5.0, 3.0, 5.0};
  double arr_result[] = {31.0, 27.0, 29.0, 73.0, 69.0, 67.0};
  Matrix temp_(2, 2);
  Matrix temp_2(2, 3);
  Matrix temp_3(2, 3);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_3.FillMatrixByArray(arr_result,
                           sizeof(arr_result) / sizeof(arr_result[0]));
  ASSERT_NO_THROW(temp_.MulMatrix(temp_2));
  bool result = temp_.EqMatrix(temp_3);
  ASSERT_TRUE(result);
}

TEST(Multiplication, matrix_5) {
  double arr[] = {1.0, 2.0, 3.0};
  double arr_2[] = {3.0, 2.0, 1.0};
  Matrix temp_(3, 1);
  Matrix temp_2(3, 1);
  Matrix temp_3(3, 1);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_3.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_THROW(temp_.MulMatrix(temp_2), std::invalid_argument);
  bool result = temp_.EqMatrix(temp_3);
  ASSERT_TRUE(result);
}

TEST(Transpose, matrix_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_2[] = {1.0, 3.0, 2.0, 4.0};
  Matrix temp_(2, 2);
  Matrix temp_2(2, 2);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  Matrix result = temp_.Transpose();
  ASSERT_TRUE(result.EqMatrix(temp_2));
}

TEST(Transpose, matrix_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {1.0, 4.0, 7.0, 2.0, 5.0, 8.0, 3.0, 6.0, 9.0};
  Matrix temp_(3, 3);
  Matrix temp_2(3, 3);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  Matrix result = temp_.Transpose();
  ASSERT_TRUE(result.EqMatrix(temp_2));
}

TEST(Transpose, matrix_3) {
  double arr[] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
  Matrix temp_(3, 3);
  Matrix temp_2(3, 3);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  Matrix result = temp_.Transpose();
  ASSERT_TRUE(result.EqMatrix(temp_2));
}

TEST(CalcComplements, matrix_1) {
  double arr[] = {1.0, 2.0, 3.0, 0.0, 4.0, 2.0, 5.0, 2.0, 1.0};
  double arr_res[] = {0.0, 10.0, -20.0, 4.0, -14.0, 8.0, -8.0, -2.0, 4.0};
  Matrix temp(3, 3);
  Matrix expectation(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.CalcComplements();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(CalcComplements, matrix_2) {
  double arr[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  double arr_res[] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
  Matrix temp(3, 3);
  Matrix expectation(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.CalcComplements();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(CalcComplements, matrix_3) {
  double arr[] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
  double arr_res[] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
  Matrix temp(3, 3);
  Matrix expectation(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.CalcComplements();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(CalcComplements, matrix_4) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  Matrix temp(2, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_THROW(Matrix result = temp.CalcComplements(), std::invalid_argument);
}

TEST(Determinant, matrix_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double expectation = -2.0;
  Matrix temp(2, 2);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  double result = temp.Determinant();
  ASSERT_DOUBLE_EQ(expectation, result);
}

TEST(Determinant, matrix_2) {
  double arr[] = {1.0, 2.0,  3.0,  4.0,  5.0,  6.0,  7.0,  8.0,
                  9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0};
  double expectation = 0.0;
  Matrix temp(4, 4);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  double result = temp.Determinant();
  ASSERT_DOUBLE_EQ(expectation, result);
}

TEST(Determinant, matrix_3) {
  double arr[] = {-0.12345};
  double expectation = -0.12345;
  Matrix temp(1, 1);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  double result = temp.Determinant();
  ASSERT_DOUBLE_EQ(expectation, result);
}

TEST(Determinant, matrix_4) {
  double arr[] = {1.0, -2.0, 3.0, 4.0, 0.0, 6.0, -7.0, 8.0, 9.0};
  double expectation = 204.0;
  Matrix temp(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  double result = temp.Determinant();
  ASSERT_DOUBLE_EQ(expectation, result);
}

TEST(Determinant, matrix_5) {
  double arr[] = {1.0,  2.0,  3.0,  4.0,  5.0,  0.6,  17.0, 0.08, 29.0,
                  10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 7.0,  8.0,
                  9.0,  20.0, 1.0,  2.0,  3.0,  4.0,  25.0};
  double expectation = -91680.0;
  Matrix temp(5, 5);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  double result = temp.Determinant();
  ASSERT_DOUBLE_EQ(expectation, result);
}

TEST(Determinant, matrix_6) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  Matrix temp(2, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_THROW(temp.Determinant(), std::invalid_argument);
}

TEST(Inverse, matrix_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_res[] = {-2.0, 1.0, 1.5, -0.5};
  Matrix temp(2, 2);
  Matrix expectation(2, 2);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.InverseMatrix();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(Inverse, matrix_2) {
  double arr[] = {2.0, 5.0, 7.0, 6.0, 3.0, 4.0, 5.0, -2.0, -3.0};
  double arr_res[] = {1.0, -1.0, 1.0, -38.0, 41.0, -34.0, 27.0, -29.0, 24.0};
  Matrix temp(3, 3);
  Matrix expectation(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.InverseMatrix();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(Inverse, matrix_3) {
  double arr[] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
  double arr_res[] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
  Matrix temp(3, 3);
  Matrix expectation(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.InverseMatrix();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(Inverse, matrix_4) {
  double arr[] = {-4.0, 0.0, 0.0, -4.0};
  double arr_res[] = {-0.25, 0.0, 0.0, -0.25};
  Matrix temp(2, 2);
  Matrix expectation(2, 2);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.InverseMatrix();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(Inverse, matrix_5) {
  double arr[] = {1.0, 2.0, 1.0, 3.0, 0.0, 5.0, 2.0, 2.0, 1.0};
  double arr_res[] = {-1.0, 0.0, 1.0, 0.7, -0.1, -0.2, 0.6, 0.2, -0.6};
  Matrix temp(3, 3);
  Matrix expectation(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp.InverseMatrix();
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(Inverse, matrix_6) {
  double arr[] = {1.0, 2.0, 1.0, 0.0, 0.0, 0.0, 2.0, 2.0, 1.0};
  Matrix temp(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_THROW(temp.InverseMatrix(), std::invalid_argument);
}

TEST(Inverse, matrix_7) {
  double arr[] = {1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 2.0, 2.0, 1.0};
  Matrix temp(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_THROW(temp.InverseMatrix(), std::invalid_argument);
}

TEST(Operators, plus_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_res[] = {2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(3, 3);
  Matrix expectation(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp_1 + temp_2;
  ASSERT_TRUE(result.EqMatrix(expectation));
}

TEST(Operators, plus_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {9.0, 8.0, 7.0, 6.0, 5.0, 4.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(2, 2);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  Matrix result = temp_1 + temp_2;
  ASSERT_EQ(result.get_rows(), 0);
  ASSERT_EQ(result.get_cols(), 0);
  ASSERT_TRUE(result.MatrixIsNull());
}

TEST(Operators, minus_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0};
  double arr_res[] = {-8.0, -6.0, -4.0, -2.0, 0.0, 2.0, 4.0, 6.0, 8.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(3, 3);
  Matrix temp_expect(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_expect.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp_1 - temp_2;
  ASSERT_EQ(result.get_rows(), 3);
  ASSERT_EQ(result.get_cols(), 3);
  ASSERT_TRUE(result.EqMatrix(temp_expect));
}
TEST(Operators, minus_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {9.0, 8.0, 7.0, 6.0, 5.0, 4.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(2, 2);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  Matrix result = temp_1 - temp_2;
  ASSERT_EQ(result.get_rows(), 0);
  ASSERT_EQ(result.get_cols(), 0);
  ASSERT_TRUE(result.MatrixIsNull());
}

TEST(Operators, equal_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_TRUE(temp_1 == temp_2);
}

TEST(Operators, equal_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(2, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_FALSE(temp_1 == temp_2);
}

TEST(Operators, assignment) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  Matrix temp_1;
  Matrix temp_2(3, 3);
  temp_2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_1 = temp_2;
  ASSERT_TRUE(temp_1.EqMatrix(temp_2));
}

TEST(Operators, plus_assignment_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_res[] = {2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(3, 3);
  Matrix expectation(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  temp_1 += temp_2;
  ASSERT_TRUE(temp_1.EqMatrix(expectation));
}

TEST(Operators, plus_assignment_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {9.0, 8.0, 7.0, 6.0, 5.0, 4.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(2, 3);
  Matrix temp_expect(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_expect.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_1 += temp_2;
  ASSERT_TRUE(temp_1.EqMatrix(temp_expect));
}

TEST(Operators, minus_assignment_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {9.0, 8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0};
  double arr_res[] = {-8.0, -6.0, -4.0, -2.0, 0.0, 2.0, 4.0, 6.0, 8.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(3, 3);
  Matrix expectation(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  temp_1 -= temp_2;
  ASSERT_TRUE(temp_1.EqMatrix(expectation));
}

TEST(Operators, minus_assignment_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {9.0, 8.0, 7.0, 6.0, 5.0, 4.0};
  Matrix temp_1(3, 3);
  Matrix temp_2(2, 3);
  Matrix temp_expect(3, 3);
  temp_expect.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_1 -= temp_2;
  ASSERT_TRUE(temp_1.EqMatrix(temp_expect));
}

TEST(Operators, index_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  Matrix temp_1(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_DOUBLE_EQ(temp_1(0, 0), 1.0);
  ASSERT_DOUBLE_EQ(temp_1(2, 1), 8.0);
  ASSERT_THROW(temp_1(3, 3), std::out_of_range);
}

TEST(Operators, index_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  Matrix temp_1(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  ASSERT_DOUBLE_EQ(temp_1(0, 0), 1.0);
  ASSERT_DOUBLE_EQ(temp_1(2, 1), 8.0);
  temp_1(2, 1) = 123.0;
  ASSERT_DOUBLE_EQ(temp_1(2, 1), 123.0);
  ASSERT_THROW(temp_1(3, 3), std::out_of_range);
}

TEST(Operators, index_const_1) {
  const Matrix temp_1(3, 3);
  ASSERT_EQ(temp_1(1, 1), 0.0);
}

TEST(Operators, index_4) {
  const Matrix temp_1(3, 3);
  Matrix temp_2(3, 3);
  ASSERT_THROW(temp_1(3, 3), std::exception);
  ASSERT_THROW(temp_2(3, 3), std::exception);
}

TEST(Operators, multiplication_number_assignment) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_res[] = {2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0};
  double num = 2.0;
  Matrix temp_1(3, 3);
  Matrix expectation(3, 3);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  temp_1 *= num;
  ASSERT_TRUE(temp_1.EqMatrix(expectation));
}

TEST(Operators, multiplication_matrix_assignment_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_2[] = {0.0, 0.0, 1.0, 1.0};
  double arr_res[] = {2.0, 2.0, 4.0, 4.0};
  Matrix temp_1(2, 2);
  Matrix temp_2(2, 2);
  Matrix expectation(2, 2);
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  expectation.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  temp_1 *= temp_2;
  ASSERT_TRUE(temp_1.EqMatrix(expectation));
}

TEST(Operators, multiplication_matrix_assignment_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0};
  double arr_2[] = {0.0, 0.0, 1.0, 1.0};
  Matrix temp_1(2, 2);
  Matrix temp_2(1, 1);
  Matrix temp_expect(2, 2);
  temp_expect.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_1.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_1 *= temp_2;
  ASSERT_TRUE(temp_1.EqMatrix(temp_expect));
}

TEST(Operators, multiplication_number_1) {
  double arr[] = {1.0, 1.0, -20.0, 5.0000001};
  double arr_res[] = {-3.0, -3.0, 60.0, -15.0000003};
  double num = -3.0;
  Matrix temp(3, 3);
  Matrix temp3(3, 3);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = temp * num;
  ASSERT_TRUE(result.EqMatrix(temp3));
}

TEST(Operators, multiplication_number_2) {
  double arr[] = {1.0, 1.0, -20.0, 5.0000001};
  double arr_res[] = {-3.0, -3.0, 60.0, -15.0000003};
  double num = -3.0;
  Matrix temp(2, 2);
  Matrix temp3(2, 2);
  temp.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp3.FillMatrixByArray(arr_res, sizeof(arr_res) / sizeof(arr_res[0]));
  Matrix result = num * temp;
  ASSERT_TRUE(result.EqMatrix(temp3));
}

TEST(Operators, multiplication_matrix_1) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0};
  double arr_2[] = {-1.0, 2.0, -3.0, 5.0, 4.0, 8.0, 1.0, 2.0, 2.0};
  double arr_result[] = {12.0, 16.0, 19.0, 27.0, 40.0, 40.0, 42.0, 64.0, 61.0};
  Matrix temp_(3, 3);
  Matrix temp_2(3, 3);
  Matrix temp_3(3, 3);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  temp_3.FillMatrixByArray(arr_result,
                           sizeof(arr_result) / sizeof(arr_result[0]));
  Matrix result;
  ASSERT_NO_THROW(result = temp_ * temp_2);
  ASSERT_TRUE(result.EqMatrix(temp_3));
}

TEST(Operators, multiplication_matrix_2) {
  double arr[] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
  double arr_2[] = {-1.0, 2.0, -3.0, 5.0, 4.0, 8.0};
  Matrix temp_(3, 2);
  Matrix temp_2(3, 2);
  temp_.FillMatrixByArray(arr, sizeof(arr) / sizeof(arr[0]));
  temp_2.FillMatrixByArray(arr_2, sizeof(arr_2) / sizeof(arr_2[0]));
  Matrix result;
  result = temp_ * temp_2;
  ASSERT_EQ(result.get_rows(), 0);
  ASSERT_EQ(result.get_cols(), 0);
  ASSERT_TRUE(result.MatrixIsNull());
}

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
